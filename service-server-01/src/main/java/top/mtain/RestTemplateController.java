package top.mtain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Ribbon+RestTemplate服务调用
 * 1. 客户端(调用端)：添加Ribbon的依赖
 * 2. 客户端(调用端)：启动类添加@EnableDiscoveryClient
 * 3. 客户端(调用端)：RestTemplate通过服务名调用服务端接口
 * 服务端(提供方)无需做任何修改
 */
@RestController
@RequestMapping("/")
public class RestTemplateController {
    @Autowired
    RestTemplate restTemplate;

    @Bean
    @LoadBalanced
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @GetMapping("/rest")
    String index() {
        String index = restTemplate.getForObject("http://SERVICE-USER/user/index", String.class);
        return "Hello"+index;
    }
}
