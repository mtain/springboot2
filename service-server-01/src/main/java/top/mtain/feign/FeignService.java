package top.mtain.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "SERVICE-USER")
public interface FeignService {
    @RequestMapping("/user/index")
    String index();
}