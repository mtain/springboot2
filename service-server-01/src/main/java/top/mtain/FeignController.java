package top.mtain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.mtain.feign.FeignService;

/**
 * Feign声明式服务调用
 * 1. 客户端(调用端)：添加Feign的依赖
 * 2. 客户端(调用端)：启动类添加@EnableDiscoveryClient
 * 3. 客户端(调用端)：创建FeignService接口类
 * 4. 客户端(调用端)：引动FeignService
 * 服务端(提供方)无需做任何修改
 */
@RestController
@RequestMapping("/")
public class FeignController {
    @Autowired
    FeignService feignService;

    @GetMapping("/feign")
    String index() {
        String index = feignService.index();
        return "Hello"+index;
    }
}
