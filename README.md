# Springboot2 示例

## 模块说明
| 服务       | 用途         |
| ---------- | ------------ |
| sys-eureka | 服务注册发现 |
| sys-admin  | 服务监控    |
| service-user | 测试服务  |
| service-guava | Guava学习(代码来源于网络)  |




## 服务间的调用方式
### 1. Ribbon+RestTemplate服务调用

**示例代码:** RestTemplateController
### 2. Feign声明式服务调用
**示例代码:** FeignController
