package top.mtain.guava.eventbus;

import com.google.common.eventbus.Subscribe;

public class MultiEventListener { //订阅者
 
    @Subscribe
    public void listen(OrderEvent event){
        System.out.println("MultiEventListener receive msg: "+event.getMessage());
    }
 
    @Subscribe
    public void listen(String message){
        System.out.println("MultiEventListener receive msg: "+message);
    }
}
