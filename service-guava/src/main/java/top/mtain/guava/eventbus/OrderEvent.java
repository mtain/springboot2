package top.mtain.guava.eventbus;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

//发布-订阅模式中传递的事件,是一个普通的POJO类
public class OrderEvent {
    private String message;
 
    public OrderEvent(String message) {
        this.message = message;
    }
 
    public String getMessage() {
        return message;
    }
	
	//测试方法
    public static void main(String[] args) {
        EventBus eventBus = new EventBus("jack");
        /*
         如果多个subscriber订阅了同一个事件,那么每个subscriber都将收到事件通知
         并且收到事件通知的顺序跟注册的顺序保持一致
        */
        eventBus.register(new EventListener()); //注册订阅者
        eventBus.register(new MultiEventListener());
        eventBus.post(new OrderEvent("hello")); //发布事件
        eventBus.post(new OrderEvent("world"));
        eventBus.post("!");
    }
 
}