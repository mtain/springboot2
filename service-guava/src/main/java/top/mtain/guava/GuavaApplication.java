package top.mtain.guava;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class GuavaApplication {
    public static void main(String[] args){
        ApplicationContext applicationContext = SpringApplication.run(GuavaApplication.class,args);
    }
}
