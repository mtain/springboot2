package top.mtain.guava.asynceventbus;

import com.google.common.eventbus.AsyncEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GuavaEventBusController {

    @Autowired
    private AsyncEventBus eventBus;

    @GetMapping("/asynceventbus")
    public String asynceventbus(){
        System.out.println("hello");
        eventBus.post("bbb"); // 调用执行方法的参数
        System.out.println("world");
        return "hello eventbus";
    }
}