package top.mtain.guava.asynceventbus;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Timer;
import java.util.TimerTask;

@Service("stuService")
public class StuService {

    @Autowired
    private AsyncEventBus asyncEventBus;

    @PostConstruct // 注册该类
    public void register(){
        asyncEventBus.register(this);
    }

    @AllowConcurrentEvents//线程安全
    @Subscribe // 异步执行的方法标识:需要传入String类型参数
    public void async01(String str){
        Timer timer = new Timer();// 实例化Timer类
        timer.schedule(new TimerTask() {
            public void run() {
                System.out.println("延时任务结束");
                this.cancel();
            }
        }, 5000);// 这里百毫秒

        System.out.println(str);
    }
}