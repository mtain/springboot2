//package top.mtain.quartz;
//
//public class TaskSchedulerService {
//    @Autowired
//    private Scheduler scheduler;
//
//    @Override
//    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 36000, rollbackFor = BaseException.class)
//    public void saveTask(Job record) throws BaseException {
//        /*
//         * 1.将任务记录插入T_SCHEDULEJOB 2.将任务交由Scheduler安排触发 3.如果是立即执行，则首先触发一次任务
//         */
//        try {
//            record.setTriggername("trigger" + record.getJobname());
//            record.setTriggername(record.getJobgroup());
//            record.setJobstatus("1");
//            this.save(record);
//        } catch (BaseException e) {
//            throw new BaseException("定时任务插入数据库失败");
//        }
//        try {
//            Class cls = Class.forName(record.getJobclassname());
//            cls.newInstance();
//            // 构建job信息
//            JobDetail job = JobBuilder.newJob(cls).withIdentity(record.getJobname(), record.getJobgroup())
//                    .withDescription(record.getRemark()).build();
//            // 触发时间点
//            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(record.getCronexpression());
//            Trigger trigger = TriggerBuilder.newTrigger()
//                    .withIdentity("trigger" + record.getJobname(), record.getJobgroup()).startNow()
//                    .withSchedule(cronScheduleBuilder).build();
//            // 传递的参数
//            job.getJobDataMap().put("jobtasktype", record.getJobtasktype());
//            job.getJobDataMap().put("jobtype", record.getJobtype());
//            job.getJobDataMap().put("apiurl", record.getApiurl());
//            job.getJobDataMap().put("params", record.getParams());
//            // 交由Scheduler安排触发
//            scheduler.scheduleJob(job, trigger);
//            if ("1".equals(record.getIsnowrun())) { // 如果是立即运行则首先触发一次任务
//                JobKey key = new JobKey(record.getJobname(), record.getJobgroup());
//                scheduler.triggerJob(key, job.getJobDataMap());
//            }
//        } catch (Exception e) {
//            throw new BaseException("新增任务失败");
//        }
//    }
//
//    @Override
//    public void trigger(Job record) throws BaseException {
//        long temp = System.currentTimeMillis();
//        try {
//            Class cls = Class.forName(record.getJobclassname());
//            cls.newInstance();
//            // 构建job信息
//            JobDetail job = JobBuilder.newJob(cls).withIdentity(record.getJobname()+temp, record.getJobgroup())
//                    .withDescription(record.getRemark()).build();
//            // 触发时间点
//            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule("0 0 2 * * ? *");
//            Trigger trigger = TriggerBuilder.newTrigger()
//                    .withIdentity("trigger" + record.getJobname()+temp, record.getJobgroup()).startNow()
//                    .withSchedule(cronScheduleBuilder).build();
//            // 传递的参数
//            job.getJobDataMap().put("jobtasktype", record.getJobtasktype());
//            job.getJobDataMap().put("jobtype", record.getJobtype());
//            job.getJobDataMap().put("apiurl", record.getApiurl());
//            job.getJobDataMap().put("params", record.getParams());
//            scheduler.scheduleJob(job, trigger);
//            JobKey key = new JobKey(record.getJobname()+temp, record.getJobgroup());
//            scheduler.triggerJob(key, job.getJobDataMap());
//        } catch (Exception e) {
//            throw new BaseException("触发任务失败");
//        }
//        //此处做延时处理，否则移除任务时偶尔会nullexception
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        try {
//            TriggerKey triggerKey = TriggerKey.triggerKey(record.getJobname()+temp, record.getJobgroup());
//            // 停止触发器
//            scheduler.pauseTrigger(triggerKey);
//            // 移除触发器
//            scheduler.unscheduleJob(triggerKey);
//            // 删除任务
//            scheduler.deleteJob(JobKey.jobKey(record.getJobname()+temp, record.getJobgroup()));
//            logger.info("移除临时触发：" + JobKey.jobKey(record.getJobname()+temp));
//        } catch (Exception e) {
//            throw new BaseException("移除临时触发任务失败");
//        }
//    }
//
//    @Override
//    public void pause(Job record) throws BaseException {
//        try {
//            record.setJobstatus("0");
//            this.update(record);
////			JobKey key = new JobKey(record.getJobname(), record.getJobgroup());
////			scheduler.pauseJob(key);
//            //如果暂停后恢复，会立刻执行一次，所以暂停时先删掉定时任务，恢复是新增
//            TriggerKey triggerKey = TriggerKey.triggerKey(record.getJobname(), record.getJobgroup());
//            // 停止触发器
//            scheduler.pauseTrigger(triggerKey);
//            // 移除触发器
//            scheduler.unscheduleJob(triggerKey);
//            // 删除任务
//            scheduler.deleteJob(JobKey.jobKey(record.getJobname(), record.getJobgroup()));
//            logger.info("暂停任务：" + JobKey.jobKey(record.getJobname()));
//        } catch (Exception e) {
//            throw new BaseException("暂停任务失败");
//        }
//    }
//
//    @Override
//    public void resume(Job record) throws BaseException {
//        try {
//            record.setJobstatus("1");
//            this.update(record);
////			JobKey key = new JobKey(record.getJobname(), record.getJobgroup());
////			scheduler.resumeJob(key);
//            //新增任务
//            Class cls = Class.forName(record.getJobclassname());
//            cls.newInstance();
//            // 构建job信息
//            JobDetail job = JobBuilder.newJob(cls).withIdentity(record.getJobname(), record.getJobgroup())
//                    .withDescription(record.getRemark()).build();
//            // 触发时间点
//            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(record.getCronexpression());
//            Trigger trigger = TriggerBuilder.newTrigger()
//                    .withIdentity("trigger" + record.getJobname(), record.getJobgroup()).startNow()
//                    .withSchedule(cronScheduleBuilder).build();
//            // 传递的参数
//            job.getJobDataMap().put("jobtasktype", record.getJobtasktype());
//            job.getJobDataMap().put("jobtype", record.getJobtype());
//            job.getJobDataMap().put("apiurl", record.getApiurl());
//            job.getJobDataMap().put("params", record.getParams());
//            // 交由Scheduler安排触发
//            scheduler.scheduleJob(job, trigger);
//        } catch (Exception e) {
//            throw new BaseException("恢复任务失败");
//        }
//    }
//
//    @Override
//    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 36000, rollbackFor = BaseException.class)
//    public void remove(String jobname, String jobgroup) throws BaseException {
//        try {
//            com.chinastock.quartz.model.JobKey jobKey = new com.chinastock.quartz.model.JobKey();
//            jobKey.setJobname(jobname);
//            jobKey.setJobgroup(jobgroup);
//            this.deleteByPrimaryKey(jobKey);
//            TriggerKey triggerKey = TriggerKey.triggerKey(jobname, jobgroup);
//            // 停止触发器
//            scheduler.pauseTrigger(triggerKey);
//            // 移除触发器
//            scheduler.unscheduleJob(triggerKey);
//            // 删除任务
//            scheduler.deleteJob(JobKey.jobKey(jobname, jobgroup));
//            logger.info("removeJob:" + JobKey.jobKey(jobname));
//        } catch (Exception e) {
//            throw new BaseException("移除任务失败");
//        }
//    }
//
//    @Override
//    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 36000, rollbackFor = BaseException.class)
//    public void updateTask(Job record) throws BaseException {
//        /*
//         * 1.更新 2.删除任务 新建任务
//         */
//        //更新
//        try {
//            this.update(record);
//            //移除任务
//            TriggerKey triggerKey = TriggerKey.triggerKey(record.getJobname(), record.getJobgroup());
//            // 停止触发器
//            scheduler.pauseTrigger(triggerKey);
//            // 移除触发器
//            scheduler.unscheduleJob(triggerKey);
//            // 删除任务
//            scheduler.deleteJob(JobKey.jobKey(record.getJobname(), record.getJobgroup()));
//            logger.info("removeJob:" + JobKey.jobKey(record.getJobname()));
//            //新增任务
//            Class cls = Class.forName(record.getJobclassname());
//            cls.newInstance();
//            // 构建job信息
//            JobDetail job = JobBuilder.newJob(cls).withIdentity(record.getJobname(), record.getJobgroup())
//                    .withDescription(record.getRemark()).build();
//            // 触发时间点
//            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(record.getCronexpression());
//            Trigger trigger = TriggerBuilder.newTrigger()
//                    .withIdentity("trigger" + record.getJobname(), record.getJobgroup()).startNow()
//                    .withSchedule(cronScheduleBuilder).build();
//            // 传递的参数
//            job.getJobDataMap().put("jobtasktype", record.getJobtasktype());
//            job.getJobDataMap().put("jobtype", record.getJobtype());
//            job.getJobDataMap().put("apiurl", record.getApiurl());
//            job.getJobDataMap().put("params", record.getParams());
//            // 交由Scheduler安排触发
//            scheduler.scheduleJob(job, trigger);
//            if ("1".equals(record.getIsnowrun())) { // 如果是立即运行则首先触发一次任务
//                JobKey key = new JobKey(record.getJobname(), record.getJobgroup());
//                scheduler.triggerJob(key, job.getJobDataMap());
//            }
//        } catch (Exception e) {
//            throw new BaseException("更新任务失败");
//        }
//
//    }
//}
