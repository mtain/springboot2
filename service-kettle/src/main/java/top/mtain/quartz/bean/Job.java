//package top.mtain.quartz.bean;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//
//import java.io.Serializable;
//import java.util.Date;
//
//public class Job extends JobKey implements Serializable {
//    @ApiModelProperty(value = "任务状态， 0禁用 1启用 2删除", name = "jobstatus")
//    private String jobstatus;
//
//    @ApiModelProperty(value = "任务cron表达式", name = "cronexpression")
//    private String cronexpression;
//
//    @ApiModelProperty(value = "任务描述", name = "remark")
//    private String remark;
//
//    @ApiModelProperty(value = "任务调度类型，0：接口，1：存储过程", name = "jobtasktype")
//    private String jobtasktype;
//
//    @ApiModelProperty(value = "接口地址", name = "apiurl")
//    private String apiurl;
//
//    @ApiModelProperty(value = "参数", name = "params")
//    private String params;
//
//    @ApiModelProperty(value = "任务类型，0：周期性，1：一次性", name = "jobtype")
//    private String jobtype;
//
//    @ApiModelProperty(value = "触发器名字", name = "triggername")
//    private String triggername;
//
//    @ApiModelProperty(value = "触发器分组", name = "triggergroup")
//    private String triggergroup;
//
//    @ApiModelProperty(value = "执行类名", name = "jobclassname")
//    private String jobclassname;
//
//    @ApiModelProperty(value = "是否立即运行，0：否，1：是", name = "isnowrun")
//    private String isnowrun;
//
//    @ApiModelProperty(value = "任务生效日期", name = "startdate")
//    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
//    private Date startdate;
//
//    @ApiModelProperty(value = "任务失效日期", name = "enddate")
//    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
//    private Date enddate;
//
//    private String createtimestamp;
//
//    private String updatetimestamp;
//
//    private String removetag;
//
//    private static final long serialVersionUID = 1L;
//
//}