import org.junit.Test;
import org.junit.runner.RunWith;
import org.pentaho.di.core.exception.KettleException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.mtain.KettleApplication;
import cn.hutool.core.io.FileUtil;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={KettleApplication.class})
public class TestKettle {
    @Test
    public void testOne() throws KettleException {
        File file = FileUtil.file("kettle-test01.ktr");
        String path = file.getPath();
        System.out.println(path);
        KettleEnvironment.init();
        TransMeta transMeta = new TransMeta(path);
        Trans trans = new Trans(transMeta);
//        trans.setParameterValue("stade", "2019-04-24");
        trans.prepareExecution(null);
        trans.startThreads();
        trans.waitUntilFinished();
    }
}
